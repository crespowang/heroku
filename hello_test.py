import os
import json
import requests

url = "http://crespowang.herokuapp.com"
with open("request.json") as post_file:
	post_data = json.load(post_file)
	headers = {'Content-Type':'application/json'}
	response = requests.post(url,data=json.dumps(post_data),headers=headers)
	if response.status_code == 400:
		print ("\n%s", response.text)
	elif response.status_code == 200:
		response_json = response.json()
		with open("expected_r.json") as er_file:
			er_data = json.load(er_file)
			if er_data == response_json:
				print "Good!"
			else:
				print "Opps!"



