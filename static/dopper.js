$(document).ready(function () {
    canvas_setup();
    input_setup();
});

var IMAGE_X = 0;
var IMAGE_Y = 0;
var IMAGE_H;
var IMAGE_W;
var ctx;
var init_data;
var init_data_clone;
function canvas_setup(){
    var canvas_obj = $('#dopper_canvas');
    ctx = canvas_obj[0].getContext('2d');
    var img_obj  = new Image();

    img_obj.onload = function(){
        ctx.drawImage(img_obj, IMAGE_X, IMAGE_Y);
        IMAGE_H = img_obj.height;
        IMAGE_W = img_obj.width;
        init_data = ctx.getImageData(IMAGE_X,IMAGE_Y,IMAGE_H,IMAGE_W);
        init_data_clone = new Uint8ClampedArray(init_data.data);

    }
    img_obj.src = canvas_obj.data('imgsrc');

}

function rerender(velocity){
    var temp_data = init_data.data;
    var abs_velocity = Math.exp(Math.abs(velocity))  * 255 / exp_base; // considering exponential growth, but I guess not so right, need to talk with biz analyst
//    var abs_velocity = Math.abs(velocity) * 255 / 100; without considering exponential growth
    for(var i = 0; i < temp_data.length; i += 4) {
          if (velocity > 0)
          {
            temp_data[i] = init_data_clone[i] + abs_velocity;
            temp_data[i + 2] = init_data_clone[i +2] - abs_velocity;
          }
          else
          {
            temp_data[i + 2] = init_data_clone[i + 2] + abs_velocity;
            temp_data[i] = init_data_clone[i] - abs_velocity;
          }
    }
    init_data.data = temp_data;
    ctx.putImageData(init_data, IMAGE_X,IMAGE_Y);
}

function input_setup(){
    $('#velocity_input').on('change', function(e){
        var value = $(this).val();
//        e.stopPropagation();
        $('#velocity_slider').val(value);
        rerender(value);
    })
}

function slider_change(value){
    $('#velocity_input').val(value).change();
}
