import os
from flask import Flask, request,  make_response, jsonify, abort, render_template
import logging
from logging import StreamHandler
app = Flask(__name__)

file_handler = StreamHandler()
app.logger.setLevel(logging.DEBUG)
app.logger.addHandler(file_handler)

@app.route('/', methods = ['POST'])
def post_program_filter():
	post_data = request.get_json(silent=True)
	if not post_data:
		abort(400)
	payload = post_data.get('payload')
	if not payload:
		abort(400)
	response = []
	for program in payload:
		title = program.get('title')
		if program.get('drm') and program.get('episodeCount'):
			image = None
			slug = None
			if program.get('image') and program.get('image').get('showImage'):
				image = program['image']['showImage']
			if program.get('slug'):
				slug = program['slug']
			response.append({"image":image, "slug":slug, "title":title})
	
	return jsonify({"response":response}),200
	
@app.route('/', methods = ['GET'])
def get_program_filter():
	return render_template('index.html')

@app.errorhandler(400)
def bad_request(error):
	return make_response(jsonify({'error':'Opps! Could not decode request! Are you sure about the JSON?'}), 400)
